# Python has several structures to store collections or multiple items in a single variable
# List[], Tuple(), Set{}, Dictionary{}
# Tuples - (1, 2, 3, 4, 5)
# Lists - [1, 2, 3, 4, 5]
# Sets - {1, 2, 3, 4, 5}
# Dictionary - {"name": "John", "age": 36}


# [Section] List
# Lists are similar to arrays in other languages. They are used to store multiple items in a single variable
# Lists are created using square brackets
# Lists are ordered, changeable, and allow duplicate values

names = ["John", "Jane", "Jack", "Jill", "Joe"]  # string list
programs = ["devloper career", "data science", "cyber security", "networking", "cloud computing"]  # string list
numbers = [1, 2, 3, 4, 5]  # integer list
truth_values = [True, False, True, False, True]  # boolean list
print(names)


# A list can contain different data types
sample_list = ["John", 36, True]
# print(sample_list)

# Note: Probably not a good idea to mix data types in a list
# Problems may arise if you try to use list with multiple data types for something that requires a single data type

# Getting the list size
# the len() method can be used to get the length of a list
print(len(names))

var_len = len(names)
print(var_len)

# Accessing list values/items
# List items are indexed and the indexing starts at 0
# it can be using the negative indexing
print(names[0])  # first item in the list (index 0) - John
print(names[-1])  # last item in the list (index -1) - Joe
print(names[1])  # second item in the list (index 1) - Jane

# Access the whole list
print(names)

# Accessing list values/items using a for loop
for name in names:
    print(name)

# Accessing the range of list values/items
# the range of values can be specified by specifying the start index and the end index, separated by a colon
# when specifying a range, the return value will be a new list with the specified items
print(names[1:3])  # second and third item in the list (index 1 and 2) 3 is not included/stopper - Jane, Jack


# Updating list values/items
# to change the value of a specific item, refer to the index number
print(names)
print(f"Current value of names[0] is {names[0]}")
names[0] = "Johnny"

print(f"New value of names[0] is {names[0]}")
print(names)

# [Section] List Manipulation
# List has methods that can be used to manipulate the list within the program
# append() method - adds an item to the end of the list

names.append("Ian")
print(names)

# deleting list items - the "del" keyword can be used to delete an item from the list
durations = [260, 180, 20]
durations.append(360)  # add 360 to the end of the list - [260, 180, 20, 360]
print(durations)

del durations[-1]  # delete the last item in the list (index -1) - 360
print(durations)

# insert() method - inserts an item at the specified index
# 1st parameter is the index of the item before which to insert, 2nd parameter is the value to insert
durations.insert(1, 120)  # insert 120 at index 1 - [260, 120, 180, 20]
print(durations)


# Membership checking - the "in" keyword can be used to check if an item exists in a list
# returns True if the item exists in the list, otherwise False
print(20 in durations)  # True
print(555 in durations)  # False


# Sorting a list - the sort() method sorts the list ascending by default
# sort() method has a parameter to specify if the sort should be descending
# sort() method has a parameter to specify if the sort should be case-sensitive
# sort() method has a parameter to specify if the sort should be numeric
# sort() method has a parameter to specify if the sort should be reverse
# sort() method has a parameter to specify if the sort should be stable
# sort() method has a parameter to specify if the sort should be based on a function

print(durations)
durations.sort()  # sort the list ascending
print(durations)

durations.sort(reverse=True)  # sort the list descending
print(durations)

# empty list - the clear() method empties the list
test_list = [1, 2, 3, 4, 5]
print(test_list)
test_list.clear()
print(test_list)


# [Section] Dictionary - key/value pairs
# Dictionaries are used to store data values in key:value pairs - similar to JSON/JavaScript object
# Dictionaries are created using curly brackets, and they have keys and values {key:value}
# A Dictionary is a collection which is ordered*, changeable and does not allow duplicates.

person1 = {
    "name": "Brandon",
    "age": 28,
    "occupation": "Student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

print(person1)

# To get the number of key-value pairs in a dictionary, use the len() method
print(len(person1))

# Accessing values in dictionary
# To get the item in the dictionary, the key name can be referred using square brackets([]) - person1["name"]
print(person1["name"])

# the keys() method can be used to get a list of all the keys in the dictionary
# method that returns a list of all the keys in the dictionary
print(person1.keys())

# the values() method can be used to get a list of all the values in the dictionary
# method that returns a list of all the values in the dictionary
print(person1.values())


# the items() method can be used to get a list of all the key-value pairs in the dictionary
# method that returns a list of all the key-value pairs in the dictionary
print(person1)
print(person1.items())

# Adding items to dictionary
# adding key-value pairs can be down with either a new index key and assigning a value to it or update the method
# index key
person1["nationality"] = "Filipino"
print(person1)

# update method
person1.update({"fav_food": "Sinigang"})
print(person1)


# Deleting entries = it can be done using the pop() method and the del keyword

person1.pop("fav_food")  # pop() method - removes the item with the specified key name
print(person1)

del person1["nationality"]  # del keyword - removes the item with the specified key name
print(person1)


# the clear() method empties the dictionary
person2 = {
    "name": "John",
    "age": 30,
}
print(person2)
# person2.clear()
# print(person2)

# looping through a dictionary

# prop is the key. Represents the key in the dictionary
for prop in person2:
    print(f"The value of {prop} is {person2[prop]}")


# Nested dictionary - dictionary can be nested inside another dictionary

person3 = {
    "name" : "Monika",
    "age" : 25,
    "occupation" : "poet",
    "isEnrolled" : True,
    "subjects" : ["Python", "SQL", "Django"]
}
print(person3)
# access the python subject
print(person3["subjects"][0])

# access the django subject
print(person3["subjects"][-1])

class_room = {
    "student1": person1,
    "student2": person3,
}
print(class_room)

# access the SQL subject in person3
# accessing the dictionary inside the dictionary - class_room["student2"] - student2 is the key
# accessing the list inside the dictionary - class_room["student2"]["subjects"] - subjects is the key
print(class_room["student2"]["subjects"][1])


# [Section] Functions
# Functions are blocks of code that can be executed multiple times
# A function can be used to get inputs, process the inputs and return a value
# Functions are created using the "def" keyword
# def function_name():

# defines a function called my_greeting
def my_greeting():
    print("Hello User!")


# calling the function - to use a function, it must be called and provided with the required parameters
my_greeting()


# Parameters can be added to function to have more control to what the inputs for the function will be.
def greet_user(username):
    print(f"Hello {username}!")


greet_user("Ian")


# Return Statements = the "return" keyword allow functions to return values

def addition(num1, num2):
    return num1 + num2


total_sum = addition(5, 9)
print(total_sum)


# [Section] Lambda Functions - anonymous functions that can be used for callbacks. With implicit return
# IT is just like any normal function, except that it has no name when it is defined, and its contained in one of code.

# A lambda function can take any numbers of arguments but can only have one expression

greet = lambda person: f"Hello {person}!"

print(greet("Ian"))

first_greet = greet("Brandon")
print(first_greet)


mult = lambda a, b: a * b

print(mult(5, 9))


# [Section] Classes
# Classes are used to create objects
# Classes would serve as blueprints to describe the concept of objects
# Each Object has characteristics(properties) and behaviors(method)

# To create a class, use the "class" keyword is used along with the class name that starts with a capital letter

class Car:
    # properties that all Car objects will have are defined in a method called __init__()
    def __init__(self, brand, model, year_of_make):
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make

        # properties that are hard coded
        self.fuel = "gasoline"
        self.fuel_level = 0

    # methods that all Car objects will have are defined in a method
    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}")
        print("filling the fuel tank . . .")
        self.fuel_level = 100
        print(f"New fuel level: {self.fuel_level}")

    def drive(self, distance):
        print(f"The has driven {distance} kilometers!")
        self.fuel_level -= distance
        print(f"The fuel left: {self.fuel_level}")


# instantiate a class
new_car = Car("Nissan", "GTR", 2019)
print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)

# to invoke a method, use the object name followed by the method name
new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)
print(new_car.fuel_level)














