# Activity

class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} short course program.")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")


# instantiate a class
new_camper = Camper("Ian", 276, "Data Science")

print(f"Camper Name: {new_camper.name}")
print(f"Camper Batch: {new_camper.batch}")
print(f"Camper Course Type: {new_camper.course_type}")

# to invoke a method, use the object name followed by the method name
new_camper.info()
new_camper.career_track()
